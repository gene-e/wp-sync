# Usage
1. Ensure `wp-sync` is inside the PATH (symlink or move it there)
2. Create a `wp-sync.config` file in same directory as script
3. Define your local database credentials in `wp-sync`
4. Define a backup directory location for database dumps to be stored in `wp-sync`

```bash
wp-sync --init # Rsync's remote directory to local
wp-sync --db # Copies remote database to local, backs up both
wp-sync --uploads # Rsync's remote wp-content/uploads to local
```